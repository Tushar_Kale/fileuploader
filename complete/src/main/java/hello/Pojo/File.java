package hello.Pojo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

import org.springframework.stereotype.Service;

@Service
@SuppressWarnings("serial")
@Entity
@Table(name = "Files")
public class File implements Serializable{
	
	/*
	 * Defining parameters of class File 
	 * and defining the parameter name and their properties in Database
	 */
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "File_Id", unique = true, nullable = false)
	private int fileId;
	
	@Column(name = "File_Name", unique = false, nullable = false)
	private String fileName;
	
	@Lob
	@Column(name = "File_Data", unique = false, nullable = false)
	private byte[] fileData;
	
	/*
	 * Default constructor
	 */
	public File(){
		this(null, null);
	}
			
	/*
	 * parameterized Constructor
	 * 
	 * The constructor accepts the name in string and file content in byte array
	 * and assigns the respective value to the respective parameter of the File class 
	 */
	public File(String fileName, byte[] fileData) {
		this.fileName = fileName;
		this.fileData = fileData;
	}

	/*
	 * Getters and Setters of the File class parameters
	 */
	
	public int getFileId() {
		return fileId;
	}
	
	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public byte[] getFileData() {
		return fileData;
	}

	public void setFileData(byte[] fileData) {
		this.fileData = fileData;
	}
	
	
	
}
