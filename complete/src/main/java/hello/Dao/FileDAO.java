package hello.Dao;

import hello.Pojo.File;

import java.io.FileInputStream;
import java.io.IOException;

import org.hibernate.Session;
import org.springframework.stereotype.Service;

@Service
public class FileDAO extends DAO {

	/*
	 * persistFileInDatabase: MultipartFile, File --> void
	 * 
	 * The method stores the file provided by user in the database
	 * 
	 * @param 		originalFile 	provides the file selected by user 
	 * 				file 			an object of class File which would be persisted in database
	 * 
	 * @exception 	IOException 	occurs due to failed input/output operations
	 */
	public void persistFileInDatabase(java.io.File originalFile, File file)
			throws IOException {

		FileInputStream fileInputStream=null;
        byte[] fileData = new byte[(int) originalFile.length()];
 
	    fileInputStream = new FileInputStream(originalFile);
	    fileInputStream.read(fileData);
	    fileInputStream.close();
		
		String fileName = originalFile.getName();
		

		file.setFileName(fileName);
		file.setFileData(fileData);

		Session session = getSession();
		session.beginTransaction();
		session.save(file);
		session.getTransaction().commit();

	}
}
