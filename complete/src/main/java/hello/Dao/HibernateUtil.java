package hello.Dao;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

@SuppressWarnings("deprecation")
public class HibernateUtil {

	private static final SessionFactory sessionFactory;
	 
	/*
	 * The static block is used to create the SessionFactory from hibernate.cfg.xml
	 */
    static {
        try {
            
            sessionFactory = new Configuration().configure().buildSessionFactory();
        } catch (Throwable ex) {
            System.err.println("Initial SessionFactory creation failed." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }
 
    /*
     * getSessionFactory: void --> SessionFactory
     * 
     * The method returns a SessionFactory object
     * 
     * @return		SessionFactory		returns a SessionFactory object		
     */
    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }
}
