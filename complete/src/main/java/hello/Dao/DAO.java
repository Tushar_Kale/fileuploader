package hello.Dao;

import org.hibernate.Session;

public class DAO {

	/*
	 * getSession: void --> Session
	 * 
	 * The method return a new session object from session factory
	 * 
	 * @return 	provides a new session object from the session factory
	 */
	public Session getSession() {
		return HibernateUtil.getSessionFactory().openSession();
	}

	/*
	 * default constructor
	 */
	protected DAO() {
	}

	/*
	 * begin: void --> void
	 * 
	 * The method starts a new transaction in order to make any changes in database
	 */
	protected void begin() {
		getSession().beginTransaction();
	}

	/*
	 * commit: void --> void
	 * 
	 * The method commits or saves the changes made in the database
	 */
	protected void commit() {
		getSession().getTransaction().commit();
	}

	/*
	 * rollback: void --> void
	 * 
	 * The method restore the database to previously defined state and closes the session
	 */
	protected void rollback() {
		
			getSession().getTransaction().rollback();
			getSession().close();
		
	}
	
	/*
	 * close: void --> void
	 * 
	 * The method closes the current session 
	 */
	public void close() {
		getSession().close();
	}

}
