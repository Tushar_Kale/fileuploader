package hello.CommonUtil;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public final class UtilityClass {
	
	/*
	 * UtilityClass: constructor
	 * Creating a private constructor so that this class doesnot get instanstiated
	 */
	private UtilityClass(){
		
	}

	/*
	 * persistFileInSystem: MultipartFile --> void
	 * 
	 * The method stores the File provided by user in the system
	 * 
	 * @param 		originalFile 	provides the File selected by the user
	 * 
	 * @exception 	IOException 	occurs due to failed input/output operations
	 */
	public static void persistFileInSystem(MultipartFile originalFile)
			throws IOException {

		InputStream inputStream = null;
		OutputStream outputStream = null;
		try {
			inputStream = originalFile.getInputStream();
			System.out.println("Original File name: " + originalFile.getOriginalFilename());
			outputStream = new FileOutputStream("/Users/tusharkale1/Documents/Archibus/gs-uploading-files-master/complete/file_directory/"+
					originalFile.getOriginalFilename());

			byte[] buffer = new byte[1024];
			int length = inputStream.read(buffer);

			while (length != -1) {
				outputStream.write(buffer, 0, length);
				length = inputStream.read(buffer);
			}
		} finally {
			org.apache.commons.io.IOUtils.closeQuietly(inputStream);
			org.apache.commons.io.IOUtils.closeQuietly(outputStream);
		}

	}

	/*
	 * downloadFile: MultipartFile, HttpServletResponse --> void
	 * 
	 * The method downloads the File provided by user
	 * 
	 * @param 		originalFile 			provides the File selected by the user 
	 * 				response 				used to store Http header and download the file
	 * 
	 * @exception 	IOException 			occurs due to failed input/output operations
	 * 				FileNotFoundException 	If the file selected is not available
	 */
	public static void downloadFile(File originalFile, HttpServletResponse response)
			throws FileNotFoundException, IOException {

		response.setHeader("Content-Disposition", "attachment; filename="
				+ originalFile.getName());
		IOUtils.copy(
				new FileInputStream(new File(String.valueOf(originalFile))),
				response.getOutputStream());
		response.flushBuffer();

	}

	
	/*
	 * getFileList: void --> ArrayList<File>
	 * 
	 * The method selects all the files persisted in the system and stores it in the arraylist
	 * 
	 * @return		ArrayList<File>		Returns array of file persisted in the system
	 */
	public static ArrayList<File> getFileList() {

		String directory = "/Users/tusharkale1/Documents/Archibus/gs-uploading-files-master/complete/file_directory/";

		File file = new File(directory);
		File[] listOfFiles = file.listFiles();
		ArrayList<File> fileDirectory = new ArrayList<File>();

		if (listOfFiles != null) {
			for (File f : listOfFiles) {
				if (!f.getName().contains(".DS_Store")) {
					fileDirectory.add(f);
				}
			}
		}
		return fileDirectory;
	}
}