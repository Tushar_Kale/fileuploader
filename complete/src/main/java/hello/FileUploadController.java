package hello;

import hello.CommonUtil.UtilityClass;
import hello.Dao.FileDAO;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.util.http.fileupload.FileUploadException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class FileUploadController {

	/*
	 * Instantiating objects of class FileDAO, UtilityClass and File Class to call
	 * methods of the respective classes using dependency injection
	 * Also creating string message to store the message data of model and view
	 */
	
	@Autowired(required = true)
	FileDAO fileDAO;
	
	@Autowired(required = true)
	hello.Pojo.File file;
	String message = "";
	
	//Stores the value of maximum file size allowed
	private long maxFileSize = 2147483647;
	
	/*
	 * provideUploadInfo(): void --> String
	 * 
	 * The method gets called when the url value is /upload and the request
	 * method is Get
	 * 
	 * @return 		a message to the user
	 */
	@RequestMapping(value = "/upload", method = RequestMethod.GET)
	public @ResponseBody String provideUploadInfo() {
		return "You can upload a file by posting to this same URL.";
	}

	
	/*
	 * handleFileUpload: String, MultipartFile, HttpServletResponse --> ModelAndView
	 * 
	 * The method calls a function to persist File in Database
	 * 
	 * @param 		name 			provides the name of the file entered by user 
	 * 				originalFile	provides the File selected by user 
	 * 				response 		is a variable to store the HttpServlet response i.e message in this method
	 * 
	 * @return 		ModelAndView 	an object of the model and view which stores information
	 * 								about data(model) and name of the view
	 * 
	 * @exception 	IOException 	occurs due to failed input/output operations
	 */
	@RequestMapping(value = "/upload", method = RequestMethod.POST)
	public @ResponseBody ModelAndView handleFileUpload(
			@RequestParam("name") String name,
			@RequestParam("file") MultipartFile originalFile,
			HttpServletResponse response) {
		

		ModelAndView model = new ModelAndView();
		model.setViewName("errorPage");
		if (!originalFile.isEmpty()) {

			try {
				
				if(originalFile.getSize() > maxFileSize){					
					throw new FileUploadException("The file has exceeded the maximum allowed limit."
							+ " You can upload a file of maximum size: " + maxFileSize);
				}
				
				UtilityClass.persistFileInSystem(originalFile);
				message = "You successfully uploaded the file: " + name;
				
				model.setViewName("success");
			}catch(FileUploadException e){
				e.printStackTrace();
				message = "You failed to upload the file: "
						+ originalFile.getName() + " due to following reason: "
						+ e.getMessage();
			}catch (IOException e) {
				e.printStackTrace();
				message = "You failed to upload the file: "
						+ originalFile.getName() + " due to following reason: "
						+ e.getMessage();
			} catch (Exception e) {
				e.printStackTrace();
				message = "You failed to upload the file: "
						+ originalFile.getName() + " due to following reason: "
						+ e.getMessage();				
			}
		} else {
			message = "You failed to upload the file: "
					+ originalFile.getName() + " because file was empty";
		}
		model.addObject("message", message);
		return model;
	}

	/*
	 * redirectToHomePage: void --> ModelAndView
	 * 
	 * The method is used to redirect to index.html to uplaod new files
	 * 
	 * @return 		ModelAndView 		object of ModelAndView which contains the view 
	 * 									name it needs to redirect to
	 */
	@RequestMapping(value = "/home", method = RequestMethod.GET)
	public @ResponseBody ModelAndView redirectToHomePage() {

		return new ModelAndView("redirect:/index.html");
	}
	
	/*
	 * redirectToDownloadPage: HttpServletRequest --> ModelAndView
	 * 
	 * The method first stores all the file persisted in system in an arrayList and
	 * On the basis of request parameter, the method will redirect to two different page which are:
	 * i)selectFileToPersist --> To select a file to store in DB
	 * ii)selectFileToDownload --> To select a file to download 
	 *
	 * @param		request			used to get action value of the previous page
	 * 
	 * @return 		ModelAndView	an object of the model and view which stores information
	 * 								about data(model) and name of the view
	 */
	@RequestMapping(value = "/selectFile", method = RequestMethod.GET)
	public @ResponseBody ModelAndView redirectToDownloadPage(
			HttpServletRequest request) {
		
		ModelAndView model = new ModelAndView();
		model.setViewName("errorPage");
		
		ArrayList<File> fileDirectory = UtilityClass.getFileList();
		String action = request.getParameter("action");
		if(fileDirectory != null){
			model.addObject("fileDirectory", fileDirectory);
			if(action.equals("persist")){
				model.setViewName("selectFileToPersist");
			}else if(action.equals("download")){
				model.setViewName("selectFileToDownload");
			}
		}else{
			message = "No Files are present in the system";
			model.addObject("message", message);
		}
		return model;
	}
	

	/*
	 * redirectToDownloadPage: File --> ModelAndView
	 * 
	 * The method persists the selected file in the database
	 * 
	 * @param		originalFile		represents the file selected by user in previous page
	 * 					
	 * @return 		ModelAndView		an object of the model and view which stores information
	 * 									about data(model) and name of the view
	 * 
	 * @exception 	IOException			occurs due to failed input/output operations
	 */
	@RequestMapping(value = "/persistFile", method = RequestMethod.GET)
	public @ResponseBody ModelAndView persistFileProcess(
			@RequestParam("file") File originalFile) {

		ModelAndView model = new ModelAndView();
		model.setViewName("errorPage");

		try {
			fileDAO.persistFileInDatabase(originalFile, file);
			message = "The file was stored in DB successfully";

		} catch (IOException e) {
			e.printStackTrace();
			message = "The process failed due to following reason: "
					+ e.getMessage();
		} catch (Exception e) {
			e.printStackTrace();
			message = "The process failed due to following reason: "
					+ e.getMessage();
		}
		model.setViewName("success");
		model.addObject("message", message);

		return model;
	}

	/*
	 * downloadFileProcess: File, HttpServletResponse --> void
	 * 
	 * The method download the file selected by user in the system
	 * 
	 * @param		originalFile		represents the file selected by user in previous page
	 * 				response			is used to download the file in the system
	 * 	
	 * @return 		ModelAndView		an object of the model and view which stores information
	 * 									about data(model) and name of the view
	 * 
	 * @exception 	IOException			occurs due to failed input/output operations
	 */
	@RequestMapping(value = "/downloadFile", method = RequestMethod.GET)
	public @ResponseBody void downloadFileProcess(
			@RequestParam("file") File originalFile,
			HttpServletResponse response) {
		try {
			
			UtilityClass.downloadFile(originalFile, response);
			
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/*
	 * goBack: void --> ModelAndView
	 * 
	 * The method is used to redirect to success page
	 * 
	 * @return		ModelAndView		an object of the model and view which stores
	 * 									information about name of the view
	 */
	@RequestMapping(value = "/success", method = RequestMethod.GET)
	public ModelAndView goBack(){
		
		ModelAndView model = new ModelAndView();
		model.setViewName("success");
		return model;
	}

}
