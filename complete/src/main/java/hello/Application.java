package hello;

import hello.Dao.FileDAO;
import hello.Pojo.File;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

@Configuration
@ComponentScan
@EnableAutoConfiguration
@EnableScheduling
public class Application {

	/*
	 * main: String Array --> void
	 * The main method Starts the application
	 */
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
	
	/*
	 * Configuring Beans
	 */
	@Bean
	public FileDAO fileDaoConfiguration() {
	    return new FileDAO();
	}
	
	@Bean
	public File fileConfiguration(){
		return new File();
	}

}
