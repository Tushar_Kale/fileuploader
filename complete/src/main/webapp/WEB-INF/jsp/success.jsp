<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="UTF-8">
<title>Process Completed</title>

</head>
<body>
	<h3 align="center">${message}</h3>
	<h4 align="center">Please select one of the button below:</h4>
	<br>
	<c:set var="persist" value="persist"></c:set>
	<c:set var="download" value="download"></c:set>
	<p align="center">
		<a href="selectFile?action=${persist}"><input type="button"
			value="Persist File in DB"></a>&nbsp; 
		<a href="selectFile?action=${download}"><input type="button" 
			value="Download File in System"></a>&nbsp;
		<a href="home"><input type="button" value="upload another File"></a>
	</p>
	
</body>
</html>