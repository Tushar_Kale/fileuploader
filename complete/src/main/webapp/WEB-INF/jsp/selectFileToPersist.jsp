<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Download a file</title>
</head>
<body>
	<form action="/persistFile" method="get">
		<h4 align = "center">
			<select name="file">
				<c:forEach items="${fileDirectory}" var="row">
					<option value="${row}" >${row.getName()}</option>
				</c:forEach>
				
			</select> <input type="submit" value="Submit">
		</h4>
	</form>
</body>
</html>